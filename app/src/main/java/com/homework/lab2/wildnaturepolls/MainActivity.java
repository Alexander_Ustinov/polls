package com.homework.lab2.wildnaturepolls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayDeque;
import java.util.Deque;

public class MainActivity extends AppCompatActivity {

    private String[] questions;
    private final Deque<String> stackAnswers = new ArrayDeque<>();
    private int currentQuestionIndex;

    private RadioButton radioButton1;
    private RadioButton radioButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questions = getResources().getStringArray(R.array.questions);
        radioButton1 = findViewById(R.id.radioButton);
        radioButton2 = findViewById(R.id.radioButton2);
        changeQuestion(currentQuestionIndex);
    }

    public void getNextQuestion(View view) {
        stackAnswers.add(radioButton1.getText().toString());
        if (currentQuestionIndex + 1 < questions.length) {
            changeQuestion(++currentQuestionIndex);
        }
    }

    public void getPreviousQuestion(View view) {
        if (!stackAnswers.isEmpty()) {
            stackAnswers.pop();
        }
        if (currentQuestionIndex > 0) {
            changeQuestion(--currentQuestionIndex);
        }
    }

    private void changeQuestion(int index) {
        TextView text = findViewById(R.id.textView);
        text.setText(questions[index]);

        if (radioButton1.isChecked()) {
            radioButton1.toggle();
        }

        if (radioButton2.isChecked()) {
            radioButton2.toggle();
        }
    }
}
